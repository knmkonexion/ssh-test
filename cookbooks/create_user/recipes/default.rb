# Create new user for remote scans

user "nessus" do
  comment "default scan user"
  home "/home/nessus"
  shell "/bin/bash"
  password "$1$s9tofOhW$eOuNQQs6gkuoXmoLDRMg7."
end