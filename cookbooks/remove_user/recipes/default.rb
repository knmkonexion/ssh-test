# Remove user from remote scans

user "nessus" do
    comment "scan user nessus no longer in use"
    action  :remove
  end
