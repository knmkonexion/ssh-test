#!/bin/bash
# loop through defined list of hosts/nodes to apply config settings
# because: this works...may not be the most efficient, but she works...

 

mapfile -t hosts < hosts.txt 
mapfile -t names < names.txt 
user="centos"
key="~/.ssh/id_rsa"

for i in "${!hosts[@]}"; do
  knife bootstrap  "${hosts[$i]}" -ssh-port 22 \
    --ssh-user "$user" --sudo --i "$key" \
    --no-host-key-verify -N "${names[$i]}" --run-list "role[scan-config-on]"
done